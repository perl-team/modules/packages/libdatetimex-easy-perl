libdatetimex-easy-perl (0.092-2) UNRELEASED; urgency=medium

  * Update years of upstream copyright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 12 Jan 2025 00:15:37 +0100

libdatetimex-easy-perl (0.092-1) unstable; urgency=medium

  * Import upstream version 0.092.
  * Drop DateTime-TimeZone-2.63.patch, merged upstream.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 12 Jan 2025 00:10:25 +0100

libdatetimex-easy-perl (0.091-2) unstable; urgency=medium

  * Add Fedora patch from upstream pull request to fix test failures with
    DateTime::TimeZone 2.63. (Closes: #1084155)
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.7.0.

 -- gregor herrmann <gregoa@debian.org>  Sun, 06 Oct 2024 15:53:22 +0200

libdatetimex-easy-perl (0.091-1) unstable; urgency=medium

  * Import upstream version 0.091.
  * Update debian/upstream/metadata.
  * Update Upstream-Contact in debian/copyright.
  * Drop debian/tests/pkg-perl/smoke-skip.
    The skipped tests have been removed.

 -- gregor herrmann <gregoa@debian.org>  Tue, 04 Oct 2022 21:00:04 +0200

libdatetimex-easy-perl (0.090-2) unstable; urgency=medium

  * Add libdatetime-format-datemanip-perl to Build-Depends-Indep and
    Recommends.

 -- gregor herrmann <gregoa@debian.org>  Tue, 16 Aug 2022 18:01:39 +0200

libdatetimex-easy-perl (0.090-1) unstable; urgency=medium

  * Import upstream version 0.090.
  * Drop Adapt-tests-to-DateTime-Format-Flexible-0.34.patch which was
    taken from an upstream ticket and is applied now.
  * debian/copyright: drop obsolete third-party stanza.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.6.1.
  * Update test and runtime dependencies.
  * Skip release tests during build and autopkgtests.

 -- gregor herrmann <gregoa@debian.org>  Sat, 13 Aug 2022 19:02:52 +0200

libdatetimex-easy-perl (0.089-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * Add patch from CPAN RT to adapt tests to DateTime-Format-
    Flexible-0.34. (Closes: #993399)
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.0.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- gregor herrmann <gregoa@debian.org>  Tue, 31 Aug 2021 21:51:07 +0200

libdatetimex-easy-perl (0.089-2) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Remove version from perl (build) dependency.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Chris Butler from Uploaders. Thanks for your work!
  * Add Testsuite declaration for autopkgtest-pkg-perl.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Ryan Niebur from Uploaders. Thanks for your work!

  [ Niko Tyni ]
  * Update to debhelper compat level 10
  * Update to Standards-Version 4.1.3
  * Declare that the package does not need (fake)root to build

 -- Niko Tyni <ntyni@debian.org>  Sun, 18 Feb 2018 11:50:43 +0200

libdatetimex-easy-perl (0.089-1) unstable; urgency=low

  * New upstream release.
  * Convert to source format 3.0 (quilt).
  * Add /me to Uploaders.
  * debian/copyright: update years of upstream copyright.

 -- gregor herrmann <gregoa@debian.org>  Sat, 28 Aug 2010 01:45:16 +0200

libdatetimex-easy-perl (0.088-2) unstable; urgency=low

  * Removed (build-)dependency on libdatetime-format-datemanip-perl:
    it's only an optional requirement, and is being removed (see: #574317)
  * Add myself to Uploaders and copyright.
  * Added version 5.10.0 to perl build-dependency (required for
    ExtUtils::MakeMaker >= 6.42)
  * Upped debhelper dependency to (>= 7.2.13), required for
    Module::AutoInstall
  * Updated copyright to point to GPL-1 in common-licences.
  * Bumped Standards-Version to 3.9.1 (no changes required).

 -- Chris Butler <chrisb@debian.org>  Fri, 06 Aug 2010 11:59:07 -0400

libdatetimex-easy-perl (0.088-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Standards-Version 3.8.3 (no changes)
  * Add myself to Uploaders and Copyright
  * Rewrite control description
  * Refresh to new DEP5 copyright format
  * Remove patch, replace with an override
  * Update dependencies per upstream

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ Ryan Niebur ]
  * Update ryan52's email address

 -- Jonathan Yu <jawnsy@cpan.org>  Sat, 09 Jan 2010 12:32:07 -0500

libdatetimex-easy-perl (0.087-1) unstable; urgency=low

  * Initial Release. (Closes: #529549)

 -- Ryan Niebur <ryanryan52@gmail.com>  Thu, 28 May 2009 16:52:12 -0700
